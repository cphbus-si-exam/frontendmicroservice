module.exports = {
    rabbitmq: process.env.rabbitmq,
    eventms: process.env.eventms,
    participant_system: process.env.participant_system
}