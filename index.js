const express = require('express');
const bodyParser = require('body-parser')
const amqp = require('amqplib');
const fetch = require('node-fetch');
const qs = require('qs');

const config = require('./config');

const app = express();
// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }))
// parse application/json
app.use(bodyParser.json())
const port = 3000;



(async() => {

    const connection = await amqp.connect(config.rabbitmq);
    const channel = await connection.createChannel();
    channel.assertQueue('eventreq_queue', {durable: false });

    app.get("/event/id", async (req, res) => {
        
        const dbEvent = await (await fetch(config.eventms+'/event', {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(req.body)
        })).json() 

        res.json(dbEvent)
    })

    app.post("/event", async (req, res) => {

        console.log(req.body)

        if(typeof req.body.eventid == "string" && req.body.venue !== null ){
            console.log("patch venue", await (await fetch(config.eventms+'/event/'+req.body.eventid+"/venue", {
                method: 'PATCH',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(req.body.venue)
            })).text() )
        }

        if(typeof req.body.eventid == "string" && req.body.accommodation !== null ){
            console.log("patch accommodation", await (await fetch(config.eventms+'/event/'+req.body.eventid+"/accommodation", {
                method: 'PATCH',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(req.body.accommodation)
            })).text() )
        }

        if(typeof req.body.eventid == "string" && req.body.transport !== null ){
            console.log("patch transport", await (await fetch(config.eventms+'/event/'+req.body.eventid+"/transport", {
                method: 'PATCH',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(req.body.transport)
            })).text() )
        }

        if(typeof req.body.eventid == "string" && req.body.catering !== null ){
            console.log("patch catering", await (await fetch(config.eventms+'/event/'+req.body.eventid+"/catering", {
                method: 'PATCH',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(req.body.catering)
            })).text() )
        }

        const sent =  channel.sendToQueue("eventreq_queue", Buffer.from(JSON.stringify(req.body))); 
        
        console.log("Sent to queue", sent)

        res.json({status: true})
    })

    app.post("/event/:eventid/participent", async (req, res) => {
        console.log(req.body, req.params.eventid)

        let participants = [];
        for (let index = 0; index < req.body.length; index++) {
            const element = req.body[index];
            participants["participants["+index+"][name]"] = element.name;
            participants["participants["+index+"][email]"] = element.email;
        }

        console.log(qs.stringify({sendinvite:true, ...participants})  )

        res.json(
            await (
                await fetch(config.participant_system+"/event/"+req.params.eventid+"/participant", {
                    method: 'POST',
                    headers: {'Content-Type':'application/x-www-form-urlencoded'}, 
                    body: qs.stringify({sendinvite:true, ...participants}) 
                })
            ).json()
        )
    });


    // Setting up the public directory
    app.use(express.static('static'));

    app.listen(port, () => console.log(`listening on port ${port}!`));


})()